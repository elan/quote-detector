import logManager
import stringManager
from collections import defaultdict
import indexManager as im
import os
import pickle


def AIncludesB(a, b):
    aStart, aEnd = a[0], a[1]
    bStart, bEnd = b[0], b[1]

    if (bStart >= aStart and bEnd <= aEnd):
        return True

    return False


def getOrderedSentence(sentenceRank):
    idsTTLines = sorted(im.sentIdToLineSource[sentenceRank].values())

    return idsTTLines


def aNamedEntityIsNotThatFar(end, start, distanceNE):
    rangeMin = start - distanceNE
    rangeMax = start
    for i in range(rangeMin, rangeMax):
        if i in im.neCibleToId:
            return True

    return False


def isBetweenQuotes(end, start, distanceQuote):
    rangeMin = start - distanceQuote
    rangeMax = end + distanceQuote
    matchStart = 0
    idEnd = 0

    for i in range(rangeMin, end):
        if i in im.openQuoteCibleToId:
            matchStart = True
            idStart = i
            break

    if matchStart:
        for i in range(idStart, rangeMax):
            if i in im.closeQuoteCibleToId:
                idEnd = i

        if (idStart == idEnd) or (idEnd == start):
            return False
        elif (idStart < start and idEnd > start) or (idStart >= start and idEnd > idStart):
            return True

    return False


def cibleSource2score(idSentenceSource, empanCible):
    # idSentenceSource -> id de la phrase source
    # empanCible -> tableau de ligne de la fenetre glissante en cours (1 ligne = 1 ligne treetagger)

    # output :
    # score -> distance Levenshtein + pondération si présence Entités nommées, Quote. Si matching exact -> score max
    # $matchingSource -> string pour affichage de la phrase source avec emphase du segment qui match (<citation>es, quos saecula prisca tulerunt</citation>)

    # processus :
    # a minima -> récupérer l'id min de la première occurrence dans source d'un mot de @empanCible, pareil pour id max
    # -> calcul du score et présentation des résultats

    empanSize = len(empanCible)
    start = empanCible[0]
    idLastMatchedTokenCible = empanCible[empanSize-1]
    score = 0
    nbHits = 0
    matchingSource = ""
    sentenceSource = getOrderedSentence(idSentenceSource)
    sourceTokenRank = 0
    # TODO : vérifier comment déclarer reboursLemma & reboursTokenCoun
    reboursLemmaCount = defaultdict(
        int, im.idSentSourceToLemmaCount[idSentenceSource].copy())
    reboursTokenCount = defaultdict(
        int, im.idSentSourceToTokenCount[idSentenceSource].copy())
    # reboursTokenCount = im.idSentSourceToTokenCount[idSentenceSource].copy()
    # reboursLemmaCount = im.idSentSourceToLemmaCount[idSentenceSource].copy()

    idMatched = []
    # en clé id/rang dans le tableau @empanCible / en valeur 1 si déjà matché.
    idCibleHasMatched = defaultdict(dict)
    lastSourceMatched = True
    sentenceSourceExtended = []

    while lastSourceMatched:
        lastSourceMatched = False
        sourceTokenRank = 0
        for idWordSource in sentenceSource:
            tripletTTSource = im.ttLinesSource[idWordSource]
            sourceToken = tripletTTSource["token"]
            sourceLemma = tripletTTSource["lemmas"]
            sourceTags = tripletTTSource["tags"]
            isFullWordSource = tripletTTSource["isFullWord"]

            toEmphatize = False
            cibleTokenRank = 0

            for idWordCible in empanCible:
                hasMatched = False
                tripletTTCible = im.ttLinesCible[idWordCible]
                cibleToken = tripletTTCible["token"]
                cibleLemma = tripletTTCible["lemmas"]
                cibleTags = tripletTTCible["tags"]
                isFullWordCible = tripletTTCible["isFullWord"]

                if (isFullWordSource or isFullWordCible) and (cibleTags not in stringManager.stopPOS and sourceTags not in stringManager.stopPOS):
                    # !!!! test qu'un mot cible ne score qu'une fois
                    # if sourceLemma in reboursLemmaCount :
                    if (reboursLemmaCount.get(sourceLemma) and reboursLemmaCount[sourceLemma] > 0) or (reboursTokenCount.get(sourceToken) and reboursTokenCount[sourceToken] > 0):
                        increment = 0
                        if cibleToken == sourceToken:
                            increment = options["pointTokenEq"]
                            hasMatched = True
                            reboursTokenCount[sourceToken] -= 1

                        # exact lemmas match
                        elif cibleLemma == sourceLemma:
                            increment = options["pointLemmaEq"]
                            hasMatched = True
                            reboursLemmaCount[sourceLemma] -= 1

                        # fuzzy token match
                    elif sourceToken in im.tokensCibleToSource[cibleToken]:
                        increment = options["pointTokenFuzz"]
                        hasMatched = True
                        reboursTokenCount[sourceToken] -= 1

                        # fuzzy lemmas match
                    elif sourceLemma in im.lemmasCibleToSource[cibleLemma]:
                        increment = options["pointLemmaFuzz"]
                        hasMatched = True
                        reboursLemmaCount[sourceLemma] -= 1

                    if hasMatched:
                        if sourceTokenRank == len(sentenceSource) - 1:
                            lastSourceMatched = True
                        if idWordCible > idLastMatchedTokenCible:
                            idLastMatchedTokenCible = idWordCible

                        idMatched.append(idWordSource)
                        toEmphatize = True
                        if not cibleTokenRank in idCibleHasMatched:
                            nbHits += 1
                            score += increment
                            idCibleHasMatched.update(
                                {cibleTokenRank: increment})

                        elif idCibleHasMatched.get(cibleTokenRank, 0) < increment:
                            score += increment - \
                                idCibleHasMatched.get(cibleTokenRank)
                            idCibleHasMatched.update(
                                {cibleTokenRank: increment})

                        break

                cibleTokenRank += 1
                if (not toEmphatize and (not isFullWordSource or not isFullWordCible) and (sourceToken == cibleToken) and sourceTags not in stringManager.typos):
                    toEmphatize = True
                    # score += 1

            matchingSource += (" " + sourceToken, " <el>" +
                               sourceToken + "</el>")[toEmphatize]
            sourceTokenRank += 1

        sentenceSourceExtended = sentenceSource.copy()

        if lastSourceMatched and idSentenceSource + 1 in im.sentIdToLineSource:
            idSentenceSource += 1
            sentenceSource = getOrderedSentence(idSentenceSource)
            reboursLemmaCount = im.idSentSourceToLemmaCount[idSentenceSource].copy(
            )
            reboursTokenCount = im.idSentSourceToTokenCount[idSentenceSource].copy(
            )
        else:
            lastSourceMatched = False
    # end while lastSourceMatched

    results = defaultdict(dict)

    entityNotFar = aNamedEntityIsNotThatFar(
        idLastMatchedTokenCible, start, options["distanceNE"])
    score += (0, options["bonusEntity"])[entityNotFar]
    results["bonusEntity"] = ("non", "oui")[entityNotFar]

    quotesNotFar = isBetweenQuotes(
        idLastMatchedTokenCible, start, options["distanceQuote"])
    score += (0, options["bonusQuote"])[quotesNotFar]
    results["bonusQuote"] = ("non", "oui")[quotesNotFar]

    stringWindow = linesToToken("cible", empanCible)
    stringSource = linesToToken("source", sentenceSourceExtended)
    exactMatch = stringManager.exactMatch(
        stringWindow, stringSource, options["bonusExactMatch"])

    last = None
    sumGap = 0
    for index in idMatched:
        if last is not None:
            sumGap += index - 1 - last

        last = index

    score -= options["gapFactor"] * sumGap
    matchingSource = "<match>" + matchingSource + "</match>"
    ratioMatch = nbHits / countFullWords(empanCible) * 100
    ratioMatch = round(ratioMatch, 2)

    results["matchingSource"] = matchingSource
    results["score"] = score
    results["ratioMatch"] = ratioMatch
    results["exactMatch"] = exactMatch
    results["start"] = start
    results["idLastMatchedTokenCible"] = idLastMatchedTokenCible

    return results


def contexteMatch(target, slidingWindowTemp):
    strToPrint = ""
    empanContexte = options["empanContexteCible"]
    leftBorder = slidingWindowTemp[0]
    rightBorder = slidingWindowTemp[len(slidingWindowTemp)-1]
    leftContext = (leftBorder - empanContexte,
                   0)[leftBorder - empanContexte < 0]
    rightContext = (rightBorder + empanContexte, cibleLength -
                    1)[rightBorder + empanContexte > cibleLength]

    for idTT in range(leftContext, rightContext):
        tripletTT = im.ttLinesCible[idTT]
        token, lemmas, tags, isFullWord = tripletTT["token"], tripletTT[
            "lemmas"], tripletTT["tags"], tripletTT["isFullWord"]

        if target == "tokens":
            strToPrint += token + " "
        elif target == "lemmas":
            strToPrint += lemmas + " "
        elif target == "tags":
            strToPrint += tags + " "
        elif target == "fullLine":
            strToPrint += "<" + token + "\t" + tags + "\t" + lemmas + ">"

    return strToPrint


def countFullWords(idsTTLines):
    cpt = 0
    for idTTLine in idsTTLines:
        isFullWord = im.ttLinesCible[idTTLine]["isFullWord"]
        if isFullWord:
            cpt += 1

    return cpt


def linesToToken(where, idsTTLines):
    output = ""
    for i in idsTTLines:
        if where == "cible":
            output += im.ttLinesCible[i]["token"] + " "
        else:
            output += im.ttLinesSource[i]["token"] + " "

    return str(output)


def init(opt):
    global options
    global hitRank
    global oneHitPerSent           # $oneHitPerSent{id_sent}{"89-98"}=$keyHit
    global hitsPerQuotePattern     # $hitsPerQuotePattern{$cible}=$score
    global hitsInSource
    global idsCibleToScore
    global cibleLength
    hitsInSource = defaultdict(dict)
    oneHitPerSent = defaultdict(dict)
    hitsPerQuotePattern = defaultdict(dict)
    idsCibleToScore = defaultdict(dict)
    hitsForPrint = dict
    cibleLength = len(im.ttLinesCible)
    options = opt
    slidingWindow = []
    hitRank = 0
    idTT = 0
    empanMax = options["empan"]

    resultsPath = 'indexes/' + options["cible"] + '-minScore' + str(options["minScore"]) + '-maxlev' + str(options["maxLev"]) + "-empan" + str(
        options["empan"]) + '-gf' + str(options["gapFactor"]) + "-je" + str(options["jokerExtent"]) + '-results'

    if not options["reset"] and os.path.exists(resultsPath):
        with open(resultsPath, 'rb') as f:
            results = pickle.load(f)
            return results

    # for idTT in range(cibleLength):
    while idTT < cibleLength:
        # logManager.writeLog( "1 SLIDING_WINDOW : " + linesToToken("cible", slidingWindow) )

        tripletTT = im.ttLinesCible[idTT]
        token = tripletTT["token"]
        lemmas = tripletTT["lemmas"]
        tags = tripletTT["tags"]
        isFullWord = tripletTT["isFullWord"]

        if countFullWords(slidingWindow) >= empanMax:
            # logManager.writeLog( "2A SLIDING_WINDOW : " + linesToToken("cible", slidingWindow, ) )
            idPrefix = slidingWindow.pop(0)
            if im.ttLinesCible[idPrefix]["isFullWord"]:
                slidingWindow.append(idTT)
                # logManager.writeLog( "le premier mot IS un fullword")
            else:
                idTT -= 1
                # logManager.writeLog( "le premier mot NOT un fullword")
        else:
            # logManager.writeLog( "2B SLIDING_WINDOW : " + linesToToken("cible", slidingWindow) )
            # logManager.writeLog( "on a pas dépassé l'empan max")
            slidingWindow.append(idTT)

        # logManager.writeLog( "3 SLIDING_WINDOW : " + linesToToken("cible", slidingWindow) )

        idsFittingSentWindow = {}
        if countFullWords(slidingWindow) >= empanMax:
            # logManager.writeLog( "4 SLIDING_WINDOW : " + linesToToken("cible", slidingWindow) )
            # logManager.writeLog( "on a assez de fullword")
            for slidingIdTT in slidingWindow:
                # logManager.writeLog( "5 SLIDING_WINDOW : " + linesToToken("cible", slidingWindow) )
                slidingTripletTT = im.ttLinesCible[slidingIdTT]
                slidingToken = slidingTripletTT["token"]
                slidingLemmas = slidingTripletTT["lemmas"]
                slidingTags = slidingTripletTT["tags"]
                slidingIsFullWord = slidingTripletTT["isFullWord"]

                idsPotentialSentForToken = {}

                if slidingIsFullWord:
                    # logManager.writeLog( " SLIDING_WINDOW : " + linesToToken("cible", slidingWindow) )
                    # TODO : faire un test en inversant l'ordre des for token et for lemma
                    # On récupère les phrases dans lesquelles le lemma (ou un fuzzy) est présent
                    unambigousLemmas = ([slidingLemmas], slidingLemmas.split("|"))[
                        "|" in slidingLemmas]
                    currentTokenHasMatched = False
                    for unambigousLemma in unambigousLemmas:
                        for lemmaSource in im.lemmasCibleToSource[unambigousLemma].keys():
                            for id in im.lemmaToIdSentSource[lemmaSource].keys():
                                currentTokenHasMatched = True
                                idsPotentialSentForToken.update({id: 1})

                    # Si ça n'avait pas matché pour le lemma on récupère les phrases dans lesquelles le token (ou un fuzzy) est présent
                    if not currentTokenHasMatched and slidingToken in im.tokensCibleToSource:
                        for tokenSource in im.tokensCibleToSource[slidingToken].keys():
                            for id in im.tokenToIdSentSource[tokenSource].keys():
                                idsPotentialSentForToken.update({id: 1})

                    for idPotentialSent in idsPotentialSentForToken.keys():
                        idsFittingSentWindow.update(
                            {idPotentialSent: idsFittingSentWindow.get(idPotentialSent, 0) + 1})

            # with open("idsFittingSentWindow", 'a') as fp:
            #     json.dump(idsFittingSentWindow, fp,  indent=4)
            # pour chaque phrase de la source avec au moins autant de match que de mots dans la fenetre glissante
            # logManager.dump(idsFittingSentWindow, options["cible"] + "-idfittingsent.json")
            for idFittingSent in idsFittingSentWindow.keys():
                slidingWindowTemp = slidingWindow.copy()
                if idsFittingSentWindow[idFittingSent] >= (countFullWords(slidingWindowTemp) - 1):
                    # tous les mots pleins de l'empan sont dans la ligne > élargir match empan
                    extentOK = True
                    idExtent = idTT
                    giveExtentAChance = options["jokerExtent"]
                    while extentOK:
                        if idExtent + 1 in im.ttLinesCible.keys():
                            extentTripletTT = im.ttLinesCible[idExtent]
                            extentToken = extentTripletTT["token"]
                            extentLemmas = extentTripletTT["lemmas"]
                            extentTags = extentTripletTT["tags"]
                            extentIsFullWord = extentTripletTT["isFullWord"]
                            unambigousExtentLemmas = ([extentLemmas], extentLemmas.split("|"))[
                                "|" in extentLemmas]
                            extensionPossible = False

                            # TODO Vérifier si cette condition est adaptée
                            if not extentIsFullWord:
                                extensionPossible = True
                            else:
                                for unambigousExtentLemma in unambigousExtentLemmas:
                                    for lemmaExtentSource in im.lemmasCibleToSource[unambigousExtentLemma].keys():
                                        if idFittingSent in im.lemmaToIdSentSource[lemmaExtentSource].keys():
                                            extensionPossible = True
                                            break

                                if not extensionPossible:
                                    for tokenSource in im.tokensCibleToSource[extentToken].keys():
                                        if idFittingSent in im.tokenToIdSentSource[tokenSource].keys():
                                            extensionPossible = True
                                            break

                            if extensionPossible or (giveExtentAChance > 0):
                                slidingWindowTemp.append(idExtent + 1)
                                idExtent += 1
                                giveExtentAChance -= 1

                            else:
                                extentOK = False
                                linesFromSource = getOrderedSentence(
                                    idFittingSent)
                                strToPrint = linesToToken(
                                    "cible", slidingWindowTemp)
                                contexteToPrint = contexteMatch(
                                    "tokens", slidingWindowTemp)

                                results = cibleSource2score(
                                    idFittingSent, slidingWindowTemp)
                                startEmpan = results["start"]
                                exactMatch = results["exactMatch"]
                                endEmpan = results["idLastMatchedTokenCible"]
                                score = results["score"]
                                matchingSource = results["matchingSource"]
                                ratioMatch = results["ratioMatch"]
                                bonusQuote = results["bonusQuote"]
                                bonusEntity = results["bonusEntity"]

                                alreadyThere = False
                                for keyIdCibleToScore in idsCibleToScore.keys():
                                    keyIdsBoundaries = keyIdCibleToScore.split(
                                        "#")
                                    keyIdStart = int(keyIdsBoundaries[0])
                                    keyIdEnd = int(keyIdsBoundaries[1])
                                    # if( ( ($keyIdStart >= $startEmpan && $keyIdStart <= $endEmpan) || ($keyIdEnd >= $startEmpan && $keyIdEnd <= $endEmpan) ) && ($idsCibleToScore{$keyIdsCibleToScore} > $score) ){
                                    if ((keyIdStart >= startEmpan and keyIdStart <= endEmpan) or (keyIdEnd >= startEmpan and keyIdEnd <= endEmpan)) and (idsCibleToScore[keyIdCibleToScore] > score):
                                        alreadyThere = True
                                if (score + exactMatch >= options["minScore"]) and not alreadyThere:

                                    # logManager.writeLog(contexteToPrint)
                                    hitRank += 1
                                    hitRankObject = {
                                        "cible": strToPrint,
                                        "empan": linesToToken("cible", slidingWindow),
                                        "contexte": contexteToPrint,
                                        "source": matchingSource,
                                        "ratio": ratioMatch,
                                        "score": score,
                                        "idstart": startEmpan,
                                        "idend": endEmpan,
                                        "exactMatch": exactMatch,
                                        "bonusQuote": bonusQuote,
                                        "bonusEntity": bonusEntity

                                    }

                                    idsCibleToScore[str(
                                        startEmpan) + "#" + str(endEmpan)] = score
                                    hitsInSource[idFittingSent].update(
                                        {hitRank: hitRankObject})

                                while countFullWords(slidingWindowTemp) > options["empan"]:
                                    suffixeEmpan = slidingWindowTemp.pop(0)
                            # end else 370 extendOK
                        else:
                            # fin de fichier
                            extentOK = False
                            linesFromSource = getOrderedSentence(idFittingSent)
        idTT += 1
    ################################## FIN LECTURE CIBLE ###############

    for keySentMatch, sentMatch in hitsInSource.items():
        for keyHit, hit in sentMatch.items():
            ratio = hit["ratio"]
            source = hit["source"]
            cible = hit["cible"]
            idstart = hit["idstart"]
            idend = hit["idend"]
            exactMatch = hit["exactMatch"]
            score = hit["score"] + exactMatch

            if ratio >= options["minRatio"]:
                addCurrent = 1

                if keySentMatch in oneHitPerSent:
                    oneHitPerSentTemp = oneHitPerSent[keySentMatch].copy()
                    for keysMatch in oneHitPerSentTemp.keys():
                        lastIdsBoundaries = keysMatch.split("-")
                        lastIdstart = int(lastIdsBoundaries[0])
                        lastIdend = int(lastIdsBoundaries[1])

                        # if ((idstart >= lastIdstart) and (idstart <= lastIdend)) or ((idend >= lastIdstart) and (idend <= lastIdend)):
                        last = [lastIdstart, lastIdend]
                        current = [idstart, idend]

                        if (AIncludesB(last, current) or AIncludesB(current, last)):
                            # a changer
                            keyHitPrevious = oneHitPerSent[keySentMatch][keysMatch]
                            previousScore = hitsInSource[keySentMatch][keyHitPrevious]["score"] + \
                                hitsInSource[keySentMatch][keyHitPrevious]["exactMatch"]
                            previousRatioScore = previousScore * \
                                hitsInSource[keySentMatch][keyHitPrevious]["ratio"]
                            if score*ratio < previousRatioScore:
                                addCurrent = 0
                            else:
                                del oneHitPerSent[keySentMatch][keysMatch]

                if addCurrent == 1:
                    oneHitPerSent[keySentMatch][str(
                        idstart) + "-" + str(idend)] = keyHit

    logManager.dump(oneHitPerSent, "oneHitPerSent.json")
    logManager.dump(hitsInSource, "hitsInSource-" + options["cible"] + ".json")

    results = defaultdict(dict)

    for k, v in oneHitPerSent.items():
        logManager.writeLog("IdSentSource = " + str(k))

        keyEmpan = list(v.keys())[0]
        valueId = v.get(keyEmpan)
        results.update({k: hitsInSource[k][valueId]})

    with open(resultsPath, 'wb') as f:
        pickle.dump(results, f)

    return results
