from flask import Flask, render_template, request

import os
import indexManager
import logManager
import tagManager
import quoteManager

app = Flask(__name__)


@app.route('/')
def form():
    inputDir = './input/'
    files = []

    for element in os.listdir(inputDir):
        if os.path.isfile(inputDir+element) and element.endswith('.txt'):
            files.append(element)

    return render_template("form.html", files=files)


@app.route('/submit', methods=['POST'])
def submit():
    result = request.form
    # params
    options = {
        'source': result['source'],
        'cible': result['cible'],
        'maxLev': int(result['maxLev']),
        'reset': (False, True)[result['reset'] == "true"],
        'empan': int(result['empan']),
        'minRatio': int(result['minRatio']),
        'bonusExactMatch': 25,
        'pointTokenEq': 5,
        'pointTokenFuzz': 4,
        'pointLemmaEq': 5,
        'pointLemmaFuzz': 3,
        'gapFactor': 2,
        'jokerExtent': 2,
        'empanContexteCible': 10,
        'minScore': 20,
        'maxResults': 500,
        'bonusEntity': 9,
        'bonusQuote': 6,
        'distanceNE': 8,
        'distanceQuote': 4,
        'bonusPattern': 1,
        'bonusSubpattern': 1
    }

    planetas = logManager.initTimer("PLANETAS", 1)
    logManager.init()
    options = tagManager.init(options)
    indexManager.create(options)
    hitsForPrint = quoteManager.init(options)
    logManager.endTimer(planetas)

    return render_template("resultat.html", hitsForPrint=hitsForPrint, options=options)


app.run(debug=True)
