$("table").tablesorter({
  theme: "bootstrap",
  cssChildRow: "tablesorter-childRow",
  widthFixed: true,
  sortList: [
    [5, 1]
  ],

  // widget code contained in the jquery.tablesorter.widgets.js file
  // use the zebra stripe widget if you plan on hiding any rows (filter widget)
  // the uitheme widget is NOT REQUIRED!
  widgets: ["filter", "zebra"],
  widgetOptions: {
    // using the default zebra striping class name, so it actually isn't included in the theme variable above
    // this is ONLY needed for bootstrap theming if you are using the filter widget, because rows are hidden
    zebra: ["even", "odd"],
    // include child row content while filtering, if true
    filter_childRows: true,
    // class name applied to filter row and each input
    filter_cssFilter: 'tablesorter-filter',
    // search from beginning
    filter_startsWith: false,
    // Set this option to false to make the searches case sensitive
    filter_ignoreCase: true
  }
})