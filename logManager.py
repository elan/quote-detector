import colorama
import json
import os
import time

colorama.init(autoreset=True)
logPath = "logs.txt"
dumpPath = 'indexes/'

def init():
    if os.path.exists(logPath):
        os.remove(logPath)

def writeLog(string):
    with open(logPath, 'a+') as f:
        f.write("\n" + string)
        f.close()

def writeConsole(string, lvl):
    print(indent(lvl) + string)

def initTimer(what, lvl):
    print(colorama.Fore.GREEN +"\n"+ indent(lvl) +"######## " + what + " ################")
    return {'what': what, 'lvl': lvl, 'start': time.time()}

def endTimer(timer):
    print(colorama.Fore.GREEN + indent(timer["lvl"]) + "######## " + timer["what"] + " OK "+ colorama.Fore.WHITE +"[%s secondes]" % (round(time.time() - timer["start"], 1)) +"\n")
    return

def indent(lvl):
    str = "";
    for i in range(1, lvl):
        str += "\t"

    return str

def dump(dict, name):
    with open(dumpPath+name, 'w') as fp:
        json.dump(dict, fp,  indent=4)
    return
