# Planetas v2 (en python)

> Aide à la recherche de citation


## Installation
```
make install
```

## Usage
```
python3 app.py
```
> L'appli est dispo sur [http://localhost:5000](http://localhost:5000)

## Autres commandes
* supprimer les index, les sorties tt, les logs
```
make clean
```

## Auteurs
Sylvain Hatier
Arnaud Bey

## Licence
GNU GENERAL PUBLIC LICENSE Version 3