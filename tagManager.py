#!/usr/bin/python
import treetaggerwrapper
import os
import logManager
import warnings
warnings.simplefilter(action='ignore')


def init(options):
    tagging = logManager.initTimer("TAGGING", 2)
    reset = options["reset"]
    options["ttcible"] = tag(options["cible"], reset)
    options["ttsource"] = tag(options["source"], reset)
    logManager.endTimer(tagging)

    return options


def tag(file, reset):
    inputFilePath = "input/"+file
    outputFilePath = "output/tt/"+file+".tt"

    if not os.path.exists(inputFilePath):
        sys.exit("missing input file")

    if reset or not os.path.exists(outputFilePath):
        tagger = treetaggerwrapper.TreeTagger(
            TAGDIR='./tools/tt/', TAGLANG='la', TAGINENC="utf8", TAGOUTENC='utf8', TAGPARFILE="latin.par")
        logManager.writeConsole("- tagging de " + file + "....", 2)
        tagger.tag_file_to("input/"+file, outputFilePath)
    else:
        logManager.writeConsole("- tagging déjà fait pour " + file, 2)

    return outputFilePath
