import os
import re
import pickle
import logManager
import stringManager
from collections import defaultdict, OrderedDict

sentIdToLineSource = defaultdict(dict)
# sentIdToLineSource{idSentSource}{rangToken} = idLineSource
ttLinesSource = OrderedDict()
# ttLinesSource{idLineSource}{"token|tag|lemmas|isFullWord"} = string
ttLinesCible = OrderedDict()
# ttLinesCible{idLineCible}{"token|tag|lemmas|isFullWord"} = string
tokenToIdSentSource = defaultdict(dict)
# tokenToIdSentSource{Emathios} = { idline: 1}
idSentSourceToTokenCount = defaultdict(dict)
# idSentSourceToTokenCount{4}={Emathios: 3} -> Il y a 3 occurrences du token Emathios dans la phrase n°4 du fichier source
tokenToIdLineSource = defaultdict(dict)
# tokenToIdLineSource[Emathios]{0}=1;
ambLemmaToIdLineCible = {}
# lemmaToIdLine{Emathios}{0}= 1;
ambLemmaToIdLineSource = defaultdict(dict)
# lemmaToIdLine{Emathios}{0}= 1;
lemmaToIdSentCible = defaultdict(dict)
# lemmaToIdLine{Emathios}{0}= 1;
lemmaToIdSentSource = defaultdict(dict)
# lemmaToIdLine{Emathios}{0}= 1;
idSentSourceToLemmaCount = defaultdict(dict)
# lemmaCountToIdSource{4}{Emathios}=3; Il y a 3 occurrences du lemme Emathios dans la phrase n°4 du fichier source
hitsPerQuotePattern = defaultdict(dict)
tokensSource = {}
tokensCible = {}
tokensCibleToSource = defaultdict(dict)
lemmasCibleToSource = defaultdict(dict)
neCibleToId = defaultdict(dict)
openQuoteCibleToId = defaultdict(dict)
closeQuoteCibleToId = defaultdict(dict)

def create(options):
    global sentIdToLineSource
    global ttLinesSource
    global ttLinesCible
    global tokenToIdSentSource
    global idSentSourceToTokenCount
    global tokenToIdLineSource
    global ambLemmaToIdLineCible
    global ambLemmaToIdLineSource
    global lemmaToIdSentCible
    global lemmaToIdSentSource
    global idSentSourceToLemmaCount
    global hitsPerQuotePattern
    global tokensSource
    global tokensCible
    global tokensCibleToSource
    global lemmasCibleToSource
    global neCibleToId
    global openQuoteCibleToId
    global closeQuoteCibleToId

    indexing = logManager.initTimer("INDEXING", 2)
    source = options["source"]
    minScore = options["minScore"]
    cible = options["cible"]
    maxLev = options["maxLev"]
    reset = options["reset"]
    empan = options["empan"]
    gapFactor = options["gapFactor"]
    jokerExtent = options["jokerExtent"]
    indexPathFuzzies = 'indexes/' + cible + '-minScore' + str(minScore) + '-maxlev' + str(maxLev) + "-empan" + str(empan) + '-gf' + str(gapFactor) + "-je" + str(jokerExtent) + '-index'

    if reset or not os.path.exists(indexPathFuzzies):
        logManager.writeConsole('- création des index', 2)
        indexes = makeIndex(options)
        with open(indexPathFuzzies, 'wb') as f:
            pickle.dump(indexes, f)
    else:
        logManager.writeConsole('- les index existent déjà', 2)

    with open(indexPathFuzzies, 'rb') as f:
        logManager.endTimer(indexing)
        indexes = pickle.load(f)

        ttLinesSource = indexes[0]
        ttLinesCible = indexes[1]
        tokensCibleToSource = indexes[2]
        lemmasCibleToSource = indexes[3]
        idSentSourceToLemmaCount = indexes[4]
        lemmaToIdSentSource = indexes[5]
        lemmaToIdSentCible = indexes[6]
        ambLemmaToIdLineSource = indexes[7]
        ambLemmaToIdLineCible = indexes[8]
        tokenToIdLineSource = indexes[9]
        idSentSourceToTokenCount = indexes[10]
        sentIdToLineSource = indexes[11]
        tokenToIdSentSource = indexes[12]
        neCibleToId = indexes[13]
        openQuoteCibleToId = indexes[14]
        closeQuoteCibleToId = indexes[15]
        hitsPerQuotePattern = indexes[16]

def makeIndex(options):
    global sentIdToLineSource
    global ttLinesSource
    global ttLinesCible
    global tokenToIdSentSource
    global idSentSourceToTokenCount
    global tokenToIdLineSource
    global ambLemmaToIdLineCible
    global ambLemmaToIdLineSource
    global lemmaToIdSentCible
    global lemmaToIdSentSource
    global idSentSourceToLemmaCount
    global hitsPerQuotePattern
    global tokensSource
    global tokensCible
    global tokensCibleToSource
    global lemmasCibleToSource
    global neCibleToId
    global openQuoteCibleToId
    global closeQuoteCibleToId

    maxLev = options["maxLev"]
    source = options["ttsource"]
    cible = options["ttcible"]

    #########################################################
    ### SOURCE
    #########################################################
    with open(source, 'r') as sourceHandler:
        #lecture des triplets tt
        ttRank = 0
        sentenceRank = 0
        tokenRank = 0
        for line in sourceHandler:
            triplet = stringManager.splitLineTab(line)
            if triplet is not None:
                token = triplet["token"]
                lemmas = triplet["lemmas"]
                tags = triplet["tags"]
                tokenFullWord = stringManager.isFullWord(lemmas, tags)

                sentIdToLineSource[sentenceRank].update({tokenRank: ttRank})
                ttLinesSource[ttRank] = {"token": token, "tags": tags, "lemmas": lemmas, "isFullWord": tokenFullWord}
                tokenToIdSentSource[token].update({sentenceRank: 1})
                idSentSourceToTokenCount[sentenceRank].update({token: idSentSourceToTokenCount[sentenceRank].get(token, 0) + 1})
                tokenToIdLineSource[token].update({ttRank: 1})
                ambLemmaToIdLineSource[lemmas].update({sentenceRank: 1})

                unambigousLemmas = ([lemmas], lemmas.split("|"))["|" in lemmas]
                for unambigousLemma in unambigousLemmas:
                    lemmaToIdSentSource[unambigousLemma].update({sentenceRank: 1})
                    idSentSourceToLemmaCount[sentenceRank].update({unambigousLemma: idSentSourceToLemmaCount[sentenceRank].get(unambigousLemma, 0) + 1})

                tokenRank += 1
                ttRank += 1
                if token not in tokensSource:
                    tokensSource[token] = 1

                if tags == "sent":
                    tokenRank = 0
                    sentenceRank += 1

        sourceHandler.close()

    #########################################################
    ### CIBLE
    #########################################################
    with open(cible, 'r') as cibleHandler:
        ttRank = 0
        #lecture des triplets tt
        sentenceRank = 0;
        #variable pour les patterns
        strTokens  = ""
        strTags    = ""
        strLemmas  = ""
        linesTT    = ""
        for line in cibleHandler:
            triplet = stringManager.splitLineTab(line)
            if triplet is not None:
                tags = triplet["tags"]
                lemmas = triplet["lemmas"]
                token = triplet["token"]
                tokenFullWord = stringManager.isFullWord(lemmas, tags)
                ttLinesCible[ttRank] = {"token": token, "tags": tags, "lemmas": lemmas, "isFullWord": tokenFullWord}
                tokensCible[triplet["token"]] = 1
                lemmas = triplet["lemmas"]
                unambigousLemmas = ([lemmas], lemmas.split("|"))["|" in lemmas]
                ttRank += 1
                strTokens  = strTokens + " " + token
                strTags    = strTags + " " + tags
                strLemmas  = strLemmas + " " + lemmas
                linesTT   = linesTT + "<" + token + "\t" + tags + "\t" + lemmas + ">"
                for unambigousLemma in unambigousLemmas:
                    lemmaToIdSentCible[unambigousLemma].update({sentenceRank: 1})

                if tags == "sent":
                    scorePattern = stringManager.sentenceMatchPattern(linesTT, options)
                    if scorePattern > 0 :
                        hitsPerQuotePattern[strTokens] = scorePattern
                        logManager.writeLog(" / strTokens / " + str(strTokens) + " / scorePattern / " + str(scorePattern))
                    strTokens  = ""
                    strTags    = ""
                    strLemmas  = ""
                    linesTT    = ""

                    sentenceRank +=1

                if re.match('^((l[uv]can.+)|(lib.r)|(dico)|([ixv]+)|(aio))$', lemmas):
                    neCibleToId.update({ttRank: 1})
                elif re.match('^[«“‘‹"\'<]$', token):
                    openQuoteCibleToId.update({ttRank: 1})
                elif re.match('^[»”’›"\'>]$', token):
                    closeQuoteCibleToId.update({ttRank: 1})

        cibleHandler.close()

    #########################################################
    ### COMPARAISON CIBLE / SOURCE
    #########################################################
    for tokenCible in tokensCible:
        for tokenSource in tokensSource:
            if stringManager.isWorthy(tokenSource, tokenCible, maxLev):
                tokensCibleToSource[tokenCible].update({tokenSource: 1})

    #comparaison des hash source & cible lemmas
    for lemmaCible in lemmaToIdSentCible:
        for lemmaSource in lemmaToIdSentSource:
            if stringManager.isWorthy(lemmaSource, lemmaCible, maxLev):
                lemmasCibleToSource[lemmaCible].update({lemmaSource: 1})

    return [ttLinesSource, ttLinesCible, tokensCibleToSource, lemmasCibleToSource, idSentSourceToLemmaCount, lemmaToIdSentSource, lemmaToIdSentCible, ambLemmaToIdLineSource, ambLemmaToIdLineCible, tokenToIdLineSource, idSentSourceToTokenCount, sentIdToLineSource, tokenToIdSentSource, neCibleToId, openQuoteCibleToId, closeQuoteCibleToId, hitsPerQuotePattern]

####################
#test de sort dict
# for tokenUnique in sorted (tokensSource.keys()):
#     print(tokenUnique)
# sorted_d = dict( sorted(tokensSource.items(), key=operator.itemgetter(1),reverse=True))
# for tokenUnique in sorted_d:
#     print(str(sorted_d[tokenUnique]) + "=>" + tokenUnique)
#test de sort dict
####################
