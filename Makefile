install:
	cd tools/tt && sh install-tagger.sh
	pip3 install -r requirements.txt

clean:
	rm -f indexes/*-index
	rm -f indexes/*.json
	rm -f output/tt/*tt
	rm -f logs.txt
