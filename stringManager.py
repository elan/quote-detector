import re
import Levenshtein
from collections import defaultdict

stopwords = ["in", "et", "hic", "/", "»", "«",  "non", "nunc", ".", ","]
stopPOS = ["sent", "pun", "rel", "cc", "prep", "pron", "esse:ind"]
typos = ["sent", "pun"]
num = "(<[^\t]+\tadj:num\t[^\t]+>|<[xvi]+\t[^\t]+\t[^\t]+>)"
liber = "(<[^\t]+\t[^\t]+\tliber>)"
openQuotesAndColon = "(<[:'\"«“‘‹<]\t[^\t]+\t[^\t]+>)"
anything = "(<[^\t]+\t[^\t]+\t[^\t]+>)"
adverbe = "(<[^\t]+\t[^\t]+\t(ut|sic|sicut|apud)>)"
verbe = "(<[^\t]+\t[^\t]+\t(habeo|dico|lego)>)"
openQuotes = "(<[H'\"«“‘‹<]\t[^\t]+\t[^\t]+>)"
closeQuotes = "(<[»”’›\"'>]\t[^\t]+\t[^\t]+>)"
poet = "(<[^\t]+\t[^\t]+\t(poeta|ille|lucan[^>]+)>)"
otherPoets = "(<[^\t]+\t[^\t]+\t(v[ie]rgil[^>]+|horat[^>]+|terenti[^>]+|cicer[^>]+|iuuenal[^>]+|tulli[^>]+)>)"

masterPatterns = defaultdict(dict)
subPatterns = defaultdict(dict)

masterPatterns.update({"sic-lucanus": "(" + adverbe + "|" +
                      poet + ")(" + anything + "){0,5}(" + openQuotes + ")"})
# masterPatterns.update({"sic-lucanus": "(" + adverbe + "|" + poet + ")(" + anything +"){0,5}(" + openQuotes +")"})
subPatterns["sic-lucanus"].update({"relateur": adverbe + "(" + anything +
                                  "){0,4}(" + verbe + ")(" + anything + "){0,4}(" + openQuotes + ")"})
subPatterns["sic-lucanus"].update({"pouet": poet})
subPatterns["sic-lucanus"].update({"libro": "((" + num + ")(" + liber + ")?)|((" +
                                  liber + ")?(" + num + "))(" + anything + "){0,3}(" + openQuotes + ")"})
subPatterns["sic-lucanus"].update({"quote-complete": "(" + openQuotes +
                                  ")(<[^\t'\"«“‘‹<]+\t[^\t]+\t[^\t]+>){2,}(" + closeQuotes + ")"})

excludePatterns = ["<[^\t]+\t[^\t]+\tordo>(<[^\t]+\t[^\t]+\tsum>)?<[^\t]+\t[^\t]+\t:>",
                   "(" + otherPoets + "(" + anything + "){0,4}" + openQuotesAndColon + ")"]


def sentenceMatchPattern(linesTT, options):
    global masterPatterns
    global subPatterns
    global excludePatterns

    for excludePattern in excludePatterns:
        # permet d'interpreter une string comme une regexp
        # evalPattern = qr/$excludePattern/
        # ? regex = re.compile(excludePattern) ?
        evalPattern = excludePattern
        if re.match(evalPattern, linesTT):
            return -1

    score = 0

    for keyPattern in masterPatterns.keys():
        pattern = masterPatterns[keyPattern]
        evalPattern = pattern
        if re.match(evalPattern, linesTT):
            score += options["bonusPattern"]
            for keySubPattern in subPatterns[keyPattern].keys():
                pattern = subPatterns[keyPattern][keySubPattern]
                # $evalPattern = qr/$pattern/
                evalPattern = pattern
                if re.match(evalPattern, linesTT):
                    score += options["bonusSubpattern"]

    return score


def splitLineTab(line):
    line = line.lower()
    # test du pattern de ligne treetagger
    if re.match(r"^[^\t]+[\t][^\t]+[\t][^\t]+$", line):
        [token, tags, lemma] = line.split('\t')
        lemma = lemma.strip()
        if lemma == "<unknown>" or lemma == "-":
            lemma = token

        return {'token': token, 'tags': tags, 'lemmas': lemma}

    return None


def isFullWord(lemmas, tags):
    if tags not in stopPOS and lemmas not in stopwords:
        return True

    return False


def exactMatch(cible, source, bonusExactMatch):
    cible = re.sub('\W', ' ', cible)
    source = re.sub('\W', ' ', source)
    cible = re.sub('\s+', ' ', cible)
    source = re.sub('\s+', ' ', source)
    cible = re.sub('v', 'u', cible)
    source = re.sub('v', 'u', source)
    # $stringSource =~ s/ [»”’:;\/\\.!?\(\[\)\]\{\}=\-,›"'>«“‘‹<]//g;

    if cible in source:
        return True

    return False


def isWorthy(strSource, strCible, maxLev):
    if strSource == strCible:
        return True

    cibleLength = len(strCible)
    sourceLength = len(strSource)
    if cibleLength == 0 or sourceLength == 0:
        return False

    elif sourceLength > cibleLength:
        if sourceLength/cibleLength > (1+maxLev/100):
            return False

    elif cibleLength/sourceLength > (1+maxLev/100):
        return False

    distance = getDistancePercent(strSource, strCible)

    if maxLev > distance:
        return True

    return False


def getDistancePercent(strSource, strCible):
    dist = Levenshtein.distance(strCible, strSource)
    length = (len(strCible) + len(strSource)) / 2
    percent = dist / length * 100

    return percent
